.PHONY: all clean test

run:
	@./standup.rb

test: rubocop

bundle:
	@bundle install --quiet --jobs 4

rubocop: bundle
	@bundle exec rubocop
