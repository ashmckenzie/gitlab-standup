#!/usr/bin/env ruby

# frozen_string_literal: true

require 'date'
require_relative 'base'

PROJECTS = {
  'GitLab' => { id: 278_964, url: 'https://gitlab.com/gitlab-org/gitlab' },
  'GitLab (sec)' => { id: 15_642_544, url: 'https://gitlab.com/gitlab-org/security/gitlab' },
  'GDK' => { id: 74_823, url: 'https://gitlab.com/gitlab-org/gitlab-development-kit' },
  'GitLab Docs' => { id: 1_794_617, url: 'https://gitlab.com/gitlab-org/gitlab-docs' },
  'GitLab marketing site' => { id: 7_764, url: 'https://gitlab.com/gitlab-com/www-gitlab-com' },
  'Gitaly' => { id: 2_009_901, url: 'https://gitlab.com/gitlab-org/gitaly' },
  'GitLab Shell' => { id: 14_022, url: 'https://gitlab.com/gitlab-org/gitlab-shell' },
  'GitLab Runner' => { id: 250_833, url: 'https://gitlab.com/gitlab-org/gitlab-runner' },
  'Omnibus GitLab' => { id: 20_699,  url: 'https://gitlab.com/gitlab-org/omnibus-gitlab' },
  'GitLab Chart' => { id: 3_828_396, url: 'https://gitlab.com/gitlab-org/charts/gitlab' },
  'GitLab Pages' => { id: 734_943, url: 'https://gitlab.com/gitlab-org/gitlab-pages' },
  'triage-ops' => { id: 7_776_928, url: 'https://gitlab.com/gitlab-org/quality/triage-ops' },
  'triage-ops (sec)' => { id: 35_012_035, url: 'https://gitlab.com/gitlab-org/security/lib/triage-ops' }
}.freeze

def merge_requests_to_review # rubocop:disable Metrics/MethodLength
  @merge_requests_to_review ||= begin
    params = {
      query: "
        {
          currentUser {
            id
            reviewRequestedMergeRequests(
                state: opened,
                sort: UPDATED_DESC
              ) {
              nodes {
                title
                webUrl
                conflicts
                mergeableDiscussionsState
                mergeWhenPipelineSucceeds
                reference(full: true)
                approved
                approvedBy {
                  nodes {
                    id
                    username
                  }
                }
                mergeable
                sourceBranch
                sourceProject {
                  webUrl
                }
                milestone {
                  title
                }
                assignees {
                  nodes {
                    id
                    name
                    webUrl
                  }
                }
                reviewers {
                  nodes {
                    id
                    username
                    name
                    webUrl
                    mergeRequestInteraction {
                      reviewState
                    }
                  }
                }
                updatedAt
                headPipeline {
                  id
                  status
                }
              }
            }
          }
        }
      "
    }

    # binding.irb
    data = gitlab_graphql_get(params).first.data['data']['currentUser']
    return [] unless data

    data['reviewRequestedMergeRequests']['nodes']
  end
end

def my_wip_merge_requests # rubocop:disable Metrics/MethodLength
  @my_wip_merge_requests ||= begin
    params = {
      query: "
        {
          currentUser {
            id
            assignedMergeRequests(state: opened, sort: UPDATED_DESC) {
              nodes {
                id
                title
                webUrl
                conflicts
                mergeableDiscussionsState
                mergeWhenPipelineSucceeds
                reference(full: true)
                approved
                mergeable
                sourceBranch
                sourceProject {
                  webUrl
                }
                milestone {
                  title
                }
                assignees {
                  nodes {
                    id
                    name
                    webUrl
                  }
                }
                reviewers {
                  nodes {
                    id
                    name
                    webUrl
                  }
                }
                updatedAt
                headPipeline {
                  id
                  status
                }
              }
            }
            authoredMergeRequests(state: opened, sort: UPDATED_DESC) {
              nodes {
                id
                title
                webUrl
                conflicts
                mergeableDiscussionsState
                mergeWhenPipelineSucceeds
                reference(full: true)
                approved
                mergeable
                sourceBranch
                sourceProject {
                  webUrl
                }
                milestone {
                  title
                }
                assignees {
                  nodes {
                    id
                    name
                    webUrl
                  }
                }
                reviewers {
                  nodes {
                    id
                    name
                    webUrl
                  }
                }
                updatedAt
                headPipeline {
                  id
                  status
                }
              }
            }
          }
        }
      "
    }

    data = gitlab_graphql_get(params).first.data['data']['currentUser']
    return [] unless data

    data['assignedMergeRequests']['nodes']
  end
end

def my_all_issues # rubocop:disable Metrics/MethodLength
  @my_all_issues ||= begin
    params = {
      scope: 'all',
      state: 'opened',
      per_page: 100,
      order_by: 'milestone_due',
      sort: 'asc',
      assignee_username: gitlab_username
    }

    gitlab_get('/issues', params).first.data
  end
end

def my_in_dev_issues
  @my_in_dev_issues ||= my_all_issues.select do |issue|
    issue['labels'].any? { |label| label.downcase == 'workflow::in dev' }
  end
end

def my_non_in_dev_issues
  @my_non_in_dev_issues ||= my_all_issues.reject do |issue|
    issue['labels'].find { |label| label.downcase == 'workflow::in dev' }
  end
end

def project_shortname_from_url(url)
  PROJECTS.each do |name, data|
    return name if data[:url] == url
  end

  "FIXME (url: #{url})"
end

def project_shortname_from_id(id)
  PROJECTS.each do |name, data|
    return name if data[:id] == id
  end

  "FIXME (id: #{id})"
end

def display_merge_requests_to_review(merge_requests, header)
  puts "#{header} (#{merge_requests.count})"
  puts '------------------------------------'
  puts ":mr: #{merge_requests.count} MR(s) to review"
  puts

  merge_requests.each do |mr|
    project_url = mr['webUrl'].match(%r{(?<project_url>.+)/-/merge_requests/\d+})[:project_url]
    project = project_shortname_from_url(project_url)
    url = "[#{mr['title']}](#{mr['webUrl']})"

    puts ":mr: #{project}: Review - #{url}"
  end
end

def i_approved_merge_request?(merge_request)
  merge_request['approvedBy']['nodes'].each do |approver|
    return true if approver['username'] == gitlab_username
  end

  false
end

def my_review_state_for_merge_request(merge_request)
  merge_request['reviewers']['nodes'].each do |reviewer|
    next unless reviewer['username'] == gitlab_username

    return reviewer['mergeRequestInteraction']['reviewState']
  end
end

def merge_requests_to_review_select # rubocop:disable Metrics/MethodLength
  merge_requests = { unreviewed: [], reviewed: [], approved: [] }

  merge_requests_to_review.select do |merge_request|
    if i_approved_merge_request?(merge_request)
      merge_requests[:approved] << merge_request
      next
    end

    case my_review_state_for_merge_request(merge_request)
    when 'UNREVIEWED'
      merge_requests[:unreviewed] << merge_request
    when 'REVIEWED'
      merge_requests[:reviewed] << merge_request
    end
  end

  merge_requests
end

merge_requests_for_review = merge_requests_to_review_select

display_merge_requests_to_review(merge_requests_for_review[:unreviewed],
                                 'Merge requests for me to review (unreviewed)')
puts "\n"
display_merge_requests_to_review(merge_requests_for_review[:reviewed],
                                 'Merge requests for me to review (reviewed)')
puts "\n"
display_merge_requests_to_review(merge_requests_for_review[:approved],
                                 'Merge requests for me to review (approved)')

puts "\nMerge requests I am working on (#{my_wip_merge_requests.count})"
puts '-----------------------------------'

my_wip_merge_requests.each do |mr|
  project = project_shortname_from_url(mr['sourceProject']['webUrl'])
  url = "[#{mr['title']}](#{mr['webUrl']})"

  puts ":mr: #{project}: FILL - #{url}"
end

puts "\nIssues I am working on (#{my_in_dev_issues.count})"
puts '---------------------------'

my_in_dev_issues.each do |issue|
  project = project_shortname_from_id(issue['project_id'])
  url = "[#{issue['title']}](#{issue['web_url']})"

  puts ":issue-created: #{project}: FILL - #{url}"
end

puts "\nIssues (all other) (#{my_non_in_dev_issues.count})"
puts '---------------------------'

my_non_in_dev_issues.each do |issue|
  project = project_shortname_from_id(issue['project_id'])
  url = "[#{issue['title']}](#{issue['web_url']})"

  puts ":issue-created: #{project}: FILL - #{url}"
end
