# GitLab Standup

This project aims to make it easier to create your daily standup post.

## Prerequisite

A [GitLab PAT (personal access token)](https://gitlab.com/-/profile/personal_access_tokens)
with `api` scope  needs to be provided in the `GITLAB_TOKEN` environment
variable:

```shell
export GITLAB_TOKEN="<your-token>"
```

## Running

```shell
make
```

or:

```shell
ruby ./standup.rb
```
